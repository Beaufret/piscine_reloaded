/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_display_file.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/29 12:00:25 by rbeaufre          #+#    #+#             */
/*   Updated: 2019/06/03 13:51:55 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft.h>
#define BUF_SIZE 10

int		main(int argc, char **argv)
{
	int		vropen;
	char	c;

	if (argc == 1)
		write(2, "File name missing.\n", 19);
	else if (argc >= 3)
		write(2, "Too many arguments.\n", 20);
	else
	{
		vropen = open(argv[1], O_RDONLY);
		if (vropen < 0)
			return (0);
		while (read(vropen, &c, 1))
			write(1, &c, 1);
		close(vropen);
	}
	return (0);
}
